/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.common.system;

/**
 *
 * @author martin
 */
public class ServerMessages {
    public static final String OK_MSG = "OK";
    public static final String GET_MSG = "GET";
    public static final String HELP_MSG = "HELP";
    public static final String ERROR_ORDER = "errord";
    public static final String ERROR_SYNTAX = "errorsyn";
    public static final String ERROR_REGISTER = "errorreg";
    public static final String ERROR_OPTION = "errparam";
    public static final String ERROR_LOGIN = "errlog";
    public static final String GENERAL_ERROR = "err";
    public static final String WARNING_ERROR = "warning";
    
    public static final String NULL = "NULL";
    // Se utiliza para indicar un error de rango en el get
    public static final String BAD_RANGE = "BAD_RANGE";
    public static final String EMPTY = "EMPTY";
   
    public static final char HEADER_SEPARATOR = '-';
    public static final String REQUEST_MSG = "/001";
    public static final String RESPONSE_MSG = "/002";
    
}
